﻿using System;
using System.Collections.Generic;

namespace Bank.Data.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Phone { get; set; }

        public virtual List<Card> Cards { get; set; }
        public virtual Address Address { get; set; }

        public Client()
        {
            Cards = new List<Card>();
        }
    }
}
