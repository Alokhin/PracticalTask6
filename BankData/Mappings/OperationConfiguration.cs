﻿using Bank.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bank.Data.Mappings
{
    class OperationConfiguration : EntityTypeConfiguration<Operation>
    {
        public OperationConfiguration()
        {
            ToTable("Operations");
            HasKey(o => o.Id);

            Property(o => o.Id).HasColumnName(@"OperationID").HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.OutCardId).HasColumnName(@"OutID").HasColumnType("char")
                .HasMaxLength(16).IsRequired();
            Property(o => o.InCardId).HasColumnName(@"InID").HasColumnType("char")
                .HasMaxLength(16).IsRequired();
            Property(o => o.Amount).HasColumnName(@"Amount").HasColumnType("money")
                .IsRequired();
            Property(o => o.OperationTime).HasColumnName(@"OperationTime").HasColumnType("datetime")
                .IsRequired();
            HasRequired(o => o.InCard).WithMany(c => c.InOperations)
                .HasForeignKey(o => o.InCardId).WillCascadeOnDelete(false); 
            HasRequired(o => o.OutCard).WithMany(c => c.OutOperations)
                .HasForeignKey(o => o.OutCardId).WillCascadeOnDelete(false); 
        }
    }
}
