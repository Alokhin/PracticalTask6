﻿using Bank.Data.Models;
using System.Data.Entity.ModelConfiguration;

namespace Bank.Data.Mappings
{
    class CardConfiguration : EntityTypeConfiguration<Card>
    {
        public CardConfiguration()
        {
            ToTable("Cards");
            HasKey(c => c.Id);

            Property(c => c.Id).HasColumnName(@"CardID").HasColumnType("char")
                .IsFixedLength().HasMaxLength(16)
                .IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(c => c.ClientId).HasColumnName(@"ClientID").HasColumnType("int")
                .IsRequired();
            Property(x => x.PinCode).HasColumnName(@"PinCode").HasColumnType("char").HasMaxLength(4)
                .IsRequired();
            Property(x => x.Balance).HasColumnName(@"Balance").HasColumnType("money").IsRequired();

            // Foreign keys
            HasRequired(c => c.Client).WithMany(cl => cl.Cards)
                .HasForeignKey(c => c.ClientId).WillCascadeOnDelete(false); // FK_Cards_Clients
        }
    }
}
