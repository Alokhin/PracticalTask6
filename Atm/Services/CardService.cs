﻿using System.Data.Entity;
using System.Linq;
using Bank.Data;
using Bank.Data.Models;

namespace Bank.Atm.Services
{
    public class CardService
    {
        private BankContext _context = new BankContext();
        // Card for ATM
        private string _bankCardId = "0000000000000000";

        public Card GetCardById(string id)
        {
            var card = _context.Cards.FirstOrDefault(c => c.Id == id);
            return card;
        }

        public Card GetCardByIdAndPinCode(string id, string pinCode)
        {
            var card = _context.Cards.FirstOrDefault(c => c.Id == id && c.PinCode == pinCode);
            return card;
        }

        public void Withdraw(Card card, decimal amount)
        {
            card.OutOperations.Add(new Operation()
            {
                InCardId = _bankCardId,
                Amount = amount
            });
            card.Balance -= amount;
            var bankCard = GetCardById(_bankCardId);
            if (bankCard != null)
            {
                bankCard.Balance += amount;
            }
            _context.SaveChanges();
        }

        public void RecalculateBalances()
        {
            var cards = _context.Cards
                    .Include(c => c.InOperations)
                    .Include(c => c.OutOperations);
            foreach (var card in cards)
            {
                card.Balance = card.InOperations.Sum(op => op.Amount) -
                    card.OutOperations.Sum(op => op.Amount);
            }
            _context.SaveChanges();
        }

        public void RecalculateBalance(Card card)
        {
            card.Balance = card.InOperations.Sum(op => op.Amount) -
                card.OutOperations.Sum(op => op.Amount);
            _context.SaveChanges();
        }
    }
}
