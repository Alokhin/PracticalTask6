﻿using Bank.Data;
using Bank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bank.Atm.Services
{
    public class DbInitializer
    {
        private BankContext _ctx = new BankContext();
        private CardService _cardService = new CardService();

        public void Initialize()
        {
            if (!_ctx.Clients.Any())
            {
                ClientInitialize();
                CardInitialize();
                OperationInitialize();
                // updating balances
                _cardService.RecalculateBalances();
            }
        }

        #region partial initializations
        private void ClientInitialize()
        {
            var clients = new List<Client>()
            {
                new Client()
                {
                    FirstName = "",
                    LastName = "BANK",
                    Phone ="",
                    BirthDay = DateTime.MinValue
                },
                new Client()
                {
                    FirstName ="Tyrion",
                    LastName ="Lannister",
                    Phone = "+380501234567" ,
                    BirthDay = DateTime.Parse("1980-4-29"),
                    Address = new Address()
                    {
                        Country = "United States",
                        State = "California",
                        City = "Los Angeles",
                        AddressLine = "Green st, 71"
                    }
                },
                new Client()
                {
                    FirstName ="Jon",
                    LastName ="Snow",
                    Phone = "+380660670102" ,
                    BirthDay = DateTime.Parse("1996-02-11"),
                    Address = new Address()
                    {
                        Country = "France",
                        City = "Paris",
                        AddressLine = "Baker st, 121"
                    }
                },
                new Client()
                {
                    FirstName ="Ned",
                    LastName ="Stark",
                    Phone = "+380953332211" ,
                    BirthDay = DateTime.Parse("1966-12-21"),
                    Address = new Address()
                    {
                        Country = "Norway",
                        City = "Oslo",
                        AddressLine = "Akebergveien st, 16"
                    }
                }
            };
            _ctx.Clients.AddRange(clients);
            _ctx.SaveChanges();
        }

        private void CardInitialize()
        {
            var clients = _ctx.Clients.ToList();
            var cards = new List<Card>()
            {
                new Card()
                {
                    Id = "0000000000000000",
                    Client = clients[0],
                    PinCode = "0000"
                },
                new Card()
                {
                    Id = "1000000000000001",
                    Client = clients[1],
                    PinCode = "0001"
                },
                new Card()
                {
                    Id = "1000000000000002",
                    Client = clients[1],
                    PinCode = "0002"
                },
                new Card()
                {
                    Id = "1000000000000003" ,
                    Client = clients[1],
                    PinCode = "0003"
                },
                new Card()
                {
                    Id = "2000000000000001" ,
                    Client = clients[2],
                    PinCode = "2222"
                },
                new Card()
                {
                    Id = "3000000000000001",
                    Client = clients[3],
                    PinCode = "3333"
                },
                new Card()
                {
                    Id = "3000000000000002",
                    Client = clients[3],
                    PinCode = "3000"
                }
            };
            _ctx.Cards.AddRange(cards);
            _ctx.SaveChanges();
        }

        private void OperationInitialize()
        {
            var operations = new List<Operation>()
            {
                new Operation() { OutCardId = "0000000000000000", InCardId = "1000000000000001",
                    Amount = 10000, OperationTime = DateTime.Now.AddHours(-30)},
                new Operation() { OutCardId = "1000000000000001", InCardId = "1000000000000002",
                    Amount = 1000, OperationTime = DateTime.Now.AddHours(-24)},
                new Operation() { OutCardId = "1000000000000001", InCardId = "1000000000000003",
                    Amount = 3000, OperationTime = DateTime.Now.AddHours(-22)},
                new Operation() { OutCardId = "1000000000000003", InCardId = "3000000000000001",
                    Amount = 2500, OperationTime = DateTime.Now.AddHours(-20) },
                new Operation() { OutCardId = "1000000000000003", InCardId = "3000000000000002",
                    Amount = 500, OperationTime = DateTime.Now.AddHours(-15) },
                new Operation() { OutCardId = "3000000000000001", InCardId = "2000000000000001",
                    Amount = 2000, OperationTime = DateTime.Now.AddHours(-12) },
                new Operation() { OutCardId = "1000000000000001", InCardId = "2000000000000001",
                    Amount = 4000, OperationTime = DateTime.Now.AddHours(-8) },
                new Operation() { OutCardId = "2000000000000001", InCardId = "1000000000000002",
                    Amount = 60.0M, OperationTime = DateTime.Now.AddHours(-6) },
                new Operation() { OutCardId = "2000000000000001", InCardId = "0000000000000000",
                    Amount = 51.23M, OperationTime = DateTime.Now.AddHours(-2) }
            };
            _ctx.Operations.AddRange(operations);
            _ctx.SaveChanges();
        }
        #endregion
    }
}
