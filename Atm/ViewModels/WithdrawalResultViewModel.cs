﻿namespace Bank.Atm.ViewModels
{
    public class WithdrawalResultViewModel
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }
}