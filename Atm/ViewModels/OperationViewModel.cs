﻿using System;

namespace Bank.Atm.ViewModels
{
    public class OperationViewModel
    {
        public decimal Amount { get; set; }
        public DateTime OperationTime { get; set; }
        public decimal CardBalance { get; set; }
        public bool IsCredit { get; set; }
    }
}