﻿using System.ComponentModel.DataAnnotations;

namespace Bank.Atm.ViewModels
{
    public class CardLoginViewModel
    {
        [Required]
        [RegularExpression("[0-9]{16}",ErrorMessage = "Card number supports only 16-digit number")]
        [Display(Name = "Card number")]
        public string CardNumber { get; set; }

        [Required]
        [RegularExpression("[0-9]{4}", ErrorMessage = "Pin code supports only 4-digit number")]
        [Display(Name = "Pin code")]
        public string PinCode { get; set; }
    }
}