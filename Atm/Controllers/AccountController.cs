﻿using Bank.Atm.Services;
using Bank.Atm.ViewModels;
using System.Web.Mvc;
using System.Web.Security;

namespace Bank.Atm.Controllers
{
    public class AccountController : Controller
    {
        private CardService _cardService = new CardService();

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(CardLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var service = new CardService();
            var card = _cardService.GetCardByIdAndPinCode(model.CardNumber, model.PinCode);
            if (card == null)
            {
                ModelState.AddModelError("", "Invalid card number or pin code");
                return View(model);
            }
            FormsAuthentication.SetAuthCookie(card.Id, false);
            return RedirectToAction("Index", "Card");
        }

        [Authorize]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Card");
        }
    }
}