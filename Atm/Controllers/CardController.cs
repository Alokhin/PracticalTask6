﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bank.Atm.Services;
using Bank.Atm.ViewModels;
using Bank.Data.Models;

namespace Bank.Atm.Controllers
{
    [Authorize]
    public class CardController : Controller
    {
        private CardService _cardService = new CardService();

        private Card CurrentCard()
        {
            return _cardService.GetCardById(User.Identity.Name);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Operations()
        {
            var card = CurrentCard();
            var operations = new List<OperationViewModel>();

            OperationViewModel map(Operation operation, bool isCredit) => new OperationViewModel()
            {
                Amount = operation.Amount, OperationTime = operation.OperationTime, IsCredit = isCredit
            };

            var creditOperations = card.OutOperations.Select(o => map(o, true));
            var debitOperations = card.InOperations.Select(o => map(o, false));

            operations.AddRange(creditOperations);
            operations.AddRange(debitOperations);

            operations.Sort((o1, o2) => o1.OperationTime.CompareTo(o2.OperationTime));

            // calculating card balance after each operation
            decimal cardBalance = 0m;
            foreach(var o in operations)
            {
                cardBalance = o.CardBalance 
                    = cardBalance + o.Amount * (o.IsCredit ? -1 : 1);
            }

            return View(operations);
        }

        [HttpGet]
        public ActionResult Withdraw()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Withdraw(decimal? amount)
        {
            if (amount == null || amount <= 0)
            {
                return View(new WithdrawalResultViewModel()
                {
                    IsSuccessful = false, Message = "You must enter a positive number!"
                });
            }

            var card = CurrentCard();

            if (card.Balance < amount)
            {
                return View(new WithdrawalResultViewModel()
                {
                    IsSuccessful = false,
                    Message = string.Format("Card doesn't have enough money! Card balance is {0} $", card.Balance)
                });              
            }

            _cardService.Withdraw(card, amount.Value);

            return View(new WithdrawalResultViewModel()
            {
                IsSuccessful = true,
                Message = string.Format("You have withdrawn {0} $!", amount)
            });
        }
    }
}